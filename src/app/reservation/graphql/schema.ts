export interface Customer {
  addCustomer: {
    id: string,
  };

  getReservationsByCustomer: Array<{
    id: string,
    from: string,
    to: string,
    customer: {
      id: string,
      firstName: string,
      lastName: string,
      emailAddress: string | null,
      phoneNumber: string | null,
    },
  }>;
}

export interface Reservation {
  reservations: Array<{
    id: string,
    from: string,
    to: string,
    customer: {
      id: string,
      firstName: string,
      lastName: string,
      emailAddress: string | null,
      phoneNumber: string | null,
    },
  }>;
}
