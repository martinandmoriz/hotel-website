import { ContactFormComponent } from './contact-form/contact-form.component';
import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '', children: [
    {path: '', component: ContactFormComponent},
  ],
  },
];
