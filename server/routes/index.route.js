import express from 'express';
import reservationRoutes from './reservations.route';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/reservations', reservationRoutes);

export default router;
