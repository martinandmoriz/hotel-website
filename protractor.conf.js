/**
 * @author: Martin Fink and Moriz Martiner
 */

/**
 * look in ./config for protractor.conf.js
 */
exports.config = require('./config/protractor.conf.js').config;
