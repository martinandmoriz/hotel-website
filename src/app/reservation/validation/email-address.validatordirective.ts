import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormGroup, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[emailAddress]',
  providers: [{provide: NG_VALIDATORS, useExisting: EmailAddressValidatorDirective, multi: true}]
})
export class EmailAddressValidatorDirective implements Validator {

  public validate(form: FormGroup): ValidationErrors | null {
    if (!form.value) {
      return null;
    }

    const email = form.value.trim();

    let error = null;

    if (!/^[a-zA-Z][a-zA-Z0-9_.+]*?@[a-zA-Z]{3,}?(\.[a-zA-Z]{2,})+$/.test(email)) {
      error = 'Input is not a valid email address.';
    }

    const message = {
      email: {
        message: error,
      },
    };

    return error ? message : null;
  }
}
