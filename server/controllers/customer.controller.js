import Customer from '../models/customer.model';
import APIError from '../helpers/APIError';

function list() {
  return Customer.list()
}

/**
 * Creates a customer and saves the object to the database
 *
 * @param firstName {string}
 * @param lastName {string}
 * @param emailAddress {string}
 * @param phoneNumber {string}
 * @return {Promise<Customer, APIError>}
 */
function create(firstName, lastName, emailAddress, phoneNumber) {
  const customer = new Customer({
    firstName: firstName,
    lastName: lastName,
    emailAddress: emailAddress,
    phoneNumber: phoneNumber,
  });

  console.log(customer);

  return customer.save();
}

/**
 * @param id {string|number}
 * @return {Promise<Customer>}
 */
function get(id) {
  return Customer.get(id);
}

export default {list, get, create};
