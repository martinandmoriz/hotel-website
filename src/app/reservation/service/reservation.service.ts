import { Injectable } from '@angular/core';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { Customer, Reservation } from '../graphql/schema';
import { GetReservationsByCustomerQuery, GetReservationsQuery } from '../graphql/queries';

@Injectable()
export class ReservationService {
  private apollo: Apollo;

  constructor(apollo: Apollo) {
    this.apollo = apollo;
  }

  public get(): ApolloQueryObservable<Array<any>> {
    return this.apollo.watchQuery<Reservation>({
      query: GetReservationsQuery,
    }).map((result) => result.data.reservations) as any;
  }

  public getByCustomer(customerId): ApolloQueryObservable<Reservation> {
    return this.apollo.watchQuery<Reservation>({
      query: GetReservationsByCustomerQuery,
      variables: {
        id: customerId
      }
    }).map((result) => result.data.reservations) as any;
  }
}
