'use strict';

import express from 'express';
import reservationController from '../controllers/reservations.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(reservationController.list)
  .post(reservationController.create);

export default router;

