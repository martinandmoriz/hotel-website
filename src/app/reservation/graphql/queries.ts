import gql from 'graphql-tag';

export const GetReservationsQuery = gql`
  query GetReservations {
    reservations {
      id
      from
      to
      customer {
        id
        firstName
        lastName
        emailAddress
        phoneNumber
      }
    }
  }
`;

export const GetReservationsByCustomerQuery = gql`
  query GetReservationsByCustomer($id: ID!) {
    reservationsByCustomer(customerId: $id) {
      id
      from
      to
      customer {
        id
        firstName
        lastName
        emailAddress
        phoneNumber
      }
    }
  }
`;
