'use strict';

import Promise from 'bluebird';
import mongoose from 'mongoose';
import APIError from '../helpers/APIError';
import httpStatus from 'http-status';
import Customer from './customer.model';

const ReservationSchema = new mongoose.Schema({
  from: {
    type: String,
    required: true,
  },
  to: {
    type: String,
    required: true,
  },
  customer: {
    type: mongoose.Schema.ObjectId,
    ref: 'Customer',
    required: true,
  },
  floor: {
    type: String,
  },
  extras: [{
    type: String,
  }],
  message: {
    type: String,
  },
});

ReservationSchema.method({

});

ReservationSchema.statics = {

  /**
   * Get a Reservation by id
   *
   * @param id
   * @returns {Promise<Reservation, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((reservation) => {
        if (reservation) {
          return reservation;
        }
        const err = new APIError('Reservation not found', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      })
  },

  /**
   * @returns {Promise<Reservation[]>}
   */
  list() {
    return this.find()
      .exec();
  },

};

/**
 * @typedef ReservationSchema Reservation
 */
export default mongoose.model('Reservation', ReservationSchema);
