import { Component } from '@angular/core';
import { ImageService } from '../image.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'content',
  styleUrls: ['./contact-form.component.scss'],
  templateUrl: './contact-form.component.html',
})
export class ContactFormComponent {

  public emailSent: boolean = false;
  private message: string;
  private sender: string;

  private subject: string;

  public submit(form: NgForm) {
    if (form.valid) {
      this.sender = form.value.from;
      this.subject = form.value.subject;
      this.message = form.value.message;

      this.emailSent = true;
    }
  }
}
