import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers';

const typeDefs = `
  type Image {
    id: ID!
    uri: String!
    description: String
  }
  
  type Category {
    id: ID!
    name: String!
    images: [Image]!
  }
  
  type Customer {
    id: ID!
    firstName: String
    lastName: String
    emailAddress: String
    phoneNumber: String
  }
  
  type Reservation {
    id: ID!
    from: String
    to: String
    customer: Customer
    floor: String
    extras: [String]
    message: String
  }
  
  type Query {
    images: [Image]
    categories: [Category]
    customers: [Customer]
    customer(id: String!): Customer
    reservations: [Reservation]
    reservation(id: String!): Reservation
    reservationsByCustomer(customerId: String!): [Reservation]
  }
  
  type Mutation {
    addCustomer(
      firstName: String!,
      lastName: String!,
      emailAddress: String,
      phoneNumber: String
    ): Customer
    addReservation(
      customerId: String!,
      from: String!,
      to: String!,
      floor: String,
      extras: [String],
      message: String
    ): Reservation
  }
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

export { schema };
