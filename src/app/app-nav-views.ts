
export const views: Object[] = [
  {
    name: 'Dashboard',
    icon: 'home',
    link: ['']
  },
  {
    name: 'Images',
    icon: 'collections',
    link: ['/images'],
  },
  {
    name: 'Reservations',
    icon: 'book',
    link: ['/reservation'],
  },
  {
    name: 'Contact',
    icon: 'phone',
    link: ['/contact'],
  },
];
