import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormGroup, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[phoneNumber]',
  providers: [{provide: NG_VALIDATORS, useExisting: PhoneNumberValidatorDirective, multi: true}]
})
export class PhoneNumberValidatorDirective implements Validator {

  public validate(form: FormGroup): ValidationErrors | null {
    if (!form.value) {
      return null;
    }

    const phoneNumber = form.value.trim();

    let error = null;

    if (!/^((\+|00)\d\d)?\d{7,10}$/.test(phoneNumber)) {
      error = 'Input is not a valid phone number.';
    }

    const message = {
      phoneNumber: {
        message: error,
      },
    };

    return error ? message : null;
  }
}
