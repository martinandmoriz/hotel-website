import { Component } from '@angular/core';
import { ImageService } from '../image.service';

const IMAGES = [
  {
    title: 'Section 1',
    images: [
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
    ]
  },
  {
    title: 'Section 2',
    images: [
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
      'http://lorempixel.com/800/400/',
    ]
  },
];

@Component({
  selector: 'pictures',
  styleUrls: ['./images.component.scss'],
  templateUrl: './images.component.html',
})
export class ImagesComponent {
  public imageSections = IMAGES;
}
