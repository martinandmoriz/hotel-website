import CustomerController from '../../controllers/customer.controller';
import ReservationController from '../../controllers/reservations.controller';

const images = [
  {
    id: 1,
    uri: 'http://lorempixel.com/800/400/',
    description: 'A random image 1',
  },
  {
    id: 2,
    uri: 'http://lorempixel.com/800/400/',
    description: 'A random image 2',
  },
  {
    id: 3,
    uri: 'http://lorempixel.com/800/400/',
    description: 'A random image 3',
  },
  {
    id: 4,
    uri: 'http://lorempixel.com/800/400/',
    description: 'A random image 4',
  },
];

const categories = [
  {
    id: 1,
    name: 'Rooms',
    images: [
      images[0],
      images[1],
      images[2],
    ],
  },
  {
    id: 2,
    name: 'Reception',
    images: [
      images[3],
    ],
  },
];

export const resolvers = {
  Query: {
    images: () => {
      return images;
    },
    categories: () => {
      return categories;
    },
    customers: () => {
      return CustomerController.list();
    },
    customer: (root, {id}) => {
      return CustomerController.get(id);
    },
    reservations: () => {
      return ReservationController.list();
    },
    reservation: (root, {id}) => {
      return ReservationController.get(id);
    },
    reservationsByCustomer: (root, {customerId}) => {
      return ReservationController.getByCustomerId(customerId);
    },
  },
  Mutation: {
    addCustomer: (root, {firstName, lastName, emailAddress, phoneNumber}) => {
      return CustomerController.create(firstName, lastName, emailAddress, phoneNumber);
    },
    addReservation: (root, {customerId, from, to, floor, extras, message}) => {
      return ReservationController.create(customerId, from, to, floor, extras, message);
    },
  },
};




