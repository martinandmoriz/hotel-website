import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';
import { ReservationService } from '../service/reservation.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'my-reservations',
  styleUrls: ['./reservation-list.component.scss'],
  templateUrl: './reservation-list.component.html',
})
export class MyReservationsComponent implements OnInit {

  public reservationsObservable: Observable<any>;
  public success: boolean = false;

  private reservations: Subject<any>;

  constructor(private reservationService: ReservationService,
              private router: Router) {
    this.reservations = new Subject();
    this.reservationsObservable = this.reservations.asObservable();
  }

  public ngOnInit(): void {
    this.reservationService.get()
      .subscribe(
        (result) => {
          let data = [];

          result.forEach((item) => data.push(item));

          data.sort((a, b) =>
            new Date(a.from).getMilliseconds() - new Date(b.from).getMilliseconds());

          this.reservations.next(data);
        },
        (error) => console.log(error),
      );
  }

  protected formatDate(iso8601String: string): string {
    let date = new Date(iso8601String);

    if (!date) {
      return 'invalid date';
    }

    return date.toDateString();
  }

  protected newReservation(): void {
    this.router.navigate(['/reservation/new']);
  }
}
