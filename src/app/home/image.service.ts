import { Injectable } from '@angular/core';
import { GetImagesQuery } from './graphql/queries';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';

@Injectable()
export class ImageService {
  private imageUrls: ApolloQueryObservable<string[]>;

  constructor(private apollo: Apollo) {
  }

  public get(): ApolloQueryObservable<string[]> {
    this.imageUrls = this.apollo.watchQuery({
      query: GetImagesQuery,
    })
      .map((result) => result.data) as any;

    return this.imageUrls;
  }
}
