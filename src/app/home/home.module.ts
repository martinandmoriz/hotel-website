import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { CarouselModule } from 'angular4-carousel';
import {
  MdCardModule,
  MdListModule,
  MdInputModule,
  MdButtonModule,
  MdSnackBarModule
} from '@angular/material';

import { ROUTES } from './home.routes';

import { ContentComponent } from './content/content.component';
import { ImageService } from './image.service';

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    ContentComponent,
  ],
  providers: [
    ImageService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MdCardModule,
    MdListModule,
    MdInputModule,
    MdButtonModule,
    MdSnackBarModule,
    CarouselModule,
    // ApolloModule.forRoot(client)
  ],
})
export class HomeModule {
  public static routes = ROUTES;
}
