import { Component } from '@angular/core';
import { ImageService } from '../image.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';
import { AddCustomerMutation, AddReservationMutation } from '../graphql/mutations';
import { Router } from '@angular/router';

@Component({
  selector: 'content',
  styleUrls: ['./reservation-new.component.scss'],
  templateUrl: './reservation-new.component.html',
})
export class ReservationFormComponent {
  public floors: string[] = ['*', '1', '2', '3', '4', '5'];
  public extras: Array<{ id: string, name: string, defaultValue: boolean }> = [
    {
      id: 'premium',
      name: 'Premium',
      defaultValue: false,
    },
    {
      id: 'big_windows',
      name: 'Big windows',
      defaultValue: true,
    },
    {
      id: 'room_service',
      name: 'Room service',
      defaultValue: false,
    },
    {
      id: 'internet',
      name: 'Internet',
      defaultValue: false,
    },
  ];

  public success: boolean = true;

  public constructor(private formBuilder: FormBuilder,
                     private apollo: Apollo,
                     private router: Router) {
  }

  public submit(form: NgForm): void {
    if (form.touched && form.valid) {
      console.log(form.value);

      const values = form.value;

      values.from = new Date(values.from);
      values.to = new Date(values.to);

      let extras = [];

      this.apollo.mutate({
        mutation: AddCustomerMutation,
        variables: {
          firstName: values.customer.firstName,
          lastName: values.customer.lastName,
          emailAddress: values.customer.emailAddress,
          phoneNumber: values.customer.phoneNumber,
        }
      }).take(1)
        .subscribe({
          next: ({data: cData}) => {
            console.log('Created customer');
            console.log(cData);
            this.apollo.mutate({
              mutation: AddReservationMutation,
              variables: {
                customerId: cData.addCustomer.id,
                from: values.from.toISOString(),
                to: values.to.toISOString(),
                floor: values.floor === '*' ? null : values.floor,
                extras,
                message: values.extras,
              }
            }).take(1)
              .subscribe({
                next: ({data: rData}) => {
                  console.log('rData:');
                  console.log(rData);
                  this.router.navigate(['/reservation']);
                },
                error: (errors) => {
                  console.log('There was an error creating a reservation.');
                  console.log(errors);
                  this.success = false;
                }
              });
          },
          error: (errors) => {
            console.log('There was an error creating a customer.');
            console.log(errors);
            this.success = false;
          },
        });
    }
  }
}
