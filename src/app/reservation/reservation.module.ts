import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormErrorComponent } from './validation/form-error.component';
import { ToAfterFromValidatorDirective } from './validation/to-after-from.validatordirective';
import { PhoneNumberValidatorDirective } from './validation/phone-number.validatordirective';
import { EmailAddressValidatorDirective } from './validation/email-address.validatordirective';
import {
  MdCardModule,
  MdListModule,
  MdInputModule,
  MdButtonModule,
  MdSnackBarModule
} from '@angular/material';

import { ROUTES } from './reservation.routes';

import { ReservationService } from './service/reservation.service';
import { ReservationFormComponent } from './reservation-new/reservation-new.component';
import { MyReservationsComponent } from './reservation-list/reservation-list.component';

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    ReservationFormComponent,
    MyReservationsComponent,
    FormErrorComponent,
    ToAfterFromValidatorDirective,
    PhoneNumberValidatorDirective,
    EmailAddressValidatorDirective,
  ],
  providers: [
    ReservationService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MdCardModule,
    MdListModule,
    MdInputModule,
    MdButtonModule,
    MdSnackBarModule,
    // ApolloModule.forRoot(client)
  ],
})
export class ReservationModule {
  public static routes = ROUTES;
}
