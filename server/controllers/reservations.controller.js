'use strict';

import Reservation from '../models/reservation.model';
import Customer from '../models/customer.model';

function toISODateString(reservation) {
  reservation.from = new Date(reservation.from).toISOString();
  reservation.to = new Date(reservation.to).toISOString();

  return reservation;
}

function get(id) {
  return Customer.list()
    .then(customers => {
      let customerList = [];
      customers.forEach(customer => {
        customerList[customer.id] = customer;
      });

      return Reservation.get(id)
        .then(reservation => {
          reservation = toISODateString(reservation);

          reservation.customer = customerList[reservation.customer];

          return reservation;
        });
    });
}

function list() {
  return Customer.list()
    .then(customers => {
      let customerList = [];
      customers.forEach(customer => {
        customerList[customer.id] = customer;
      });

      return Reservation.list()
        .then(reservations => {
          reservations.forEach(reservation => {
            reservation.customer = customerList[reservation.customer];
          });

          return reservations;
        });
    });
}

function getByCustomerId(customerId) {
  return Customer.get(customerId)
    .then(customer => {
      return Reservation.list()
        .then(reservations => {
          let ret = [];

          console.log(reservations);

          reservations.forEach(reservation => {
            if (reservation.customer.toString() === customerId) {
              console.log('same');
              reservation = toISODateString(reservation);
              reservation.customer = customer;
              ret.push(reservation);
            }
          });

          return ret;
        })
    })
}

/**
 * @param customerId {string}
 * @param from {string} iso8601
 * @param to {string} iso8601
 * @param floor {string}
 * @param extras {string[]}
 * @param message {string}
 */
function create(customerId, from, to, floor, extras, message) {
  return Customer.get(customerId)
    .then(customer => {

      // TODO: check if date is valid

      const reservation = new Reservation({
        from: from,
        to: to,
        customer: customer,
        floor: floor,
        extras: extras,
        message: message,
      });

      return reservation.save();
    })
}

export default {get, list, create, getByCustomerId};
