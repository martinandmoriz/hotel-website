import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
  {path: '', loadChildren: './home#HomeModule'},
  {path: 'pictures', loadChildren: './pictures#PicturesModule'},
  {path: 'images', loadChildren: './pictures#PicturesModule'},
  {path: 'reservation', loadChildren: './reservation#ReservationModule'},
  {path: 'contact', loadChildren: './contact#ContactModule'},
  {path: '**', component: NoContentComponent},
];
