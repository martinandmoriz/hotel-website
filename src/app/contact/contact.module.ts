import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { EmailAddressValidatorDirective } from './validator/email-validator.directive';
import {
  MdCardModule,
  MdListModule,
  MdInputModule,
  MdButtonModule,
  MdSnackBarModule
} from '@angular/material';

import { ROUTES } from './contact.routes';

import { ContactFormComponent } from './contact-form/contact-form.component';

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    ContactFormComponent,
    EmailAddressValidatorDirective,
  ],
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MdCardModule,
    MdListModule,
    MdInputModule,
    MdButtonModule,
    MdSnackBarModule,
    // ApolloModule.forRoot(client)
  ],
})
export class ContactModule {
  public static routes = ROUTES;
}
