Hotel Website
=============

## Setup

Um das Projekt zu starten, müssen Node und Mongo installiert sein.

MongoDB wird mit dem Befehl `mongod` gestartet werden.

In einem neuen Terminal-Fenster kann das Projekt dann mithilfe dieser Befehle das Projekt zum Laufen gebracht werden:

```bash
$ git clone https://gitlab.com/martinandmoriz/hotel-website.git
$ cd hotel-website
$ npm install
$ npm start
```

Der Standardbrowser sollte sich automatisch starten. Geschieht dies nicht automatisch, so ist die Website auf der Adresse
`127.0.0.1:3000` zu finden.

## Projektstruktur

### Backend

Das Backend ist unter `/server` zu finden. Es ist mithilfe von Node.js und Express errichtet.
Außerdem wird GraphQl verwendet. Dies kann unter `/api/graphiql` mithilfe des graphischen Tools GraphiQl getestet werden.

Datenbankzugriffe werden mithilfe des offiziellen mongo-clients gelöst. Die Modelle für die verschiedenen
Objekte und Models ist unter `/server/models` zu finden.

Die Controller, welche die Models benutzen und auf GraphQl zugreifen, sind unter `/server/controller` zu finden.

Unter `/server/config` findet man die Konfiguration des Servers, die GraphQl Konfiguration ist unter
`/server/config/graphql` zu finden.

Werden Änderungen am Backend-Code festgestellt, so werden diese automatisch inkrementell kompiliert. Dies ist viel
schneller als eine manuelle komplette Kompilation.

### Frontend

Für das Frontend wird Angular und TypeScript verwendet, ein Superset von JavaScript, das zu JS kompiliert wird.
TypeScript hat als größtes Merkmal starke Typisierung und wird zu JavaScript kompiliert.
So kann man auch browserunabhängig neuere Sprachen-Features verwenden, da TypeScript zu ES5
kompiliert wird, welches jeder moderne Browser mittlerweile unterstützt.

Das Frontend ist under `/src/app` zu finden. Dort befinden sich die verschiedenen Module, welche teilweise
wieder in Untermodule aufgeteilt sind. Die einzelnen Komponenten bestehen dann aus einer einzelnen HTML5-Datei, einer
TypeScript-Datei, welche für die Logik zuständig ist und einer SCSS-Datei, welche zu CSS kompiliert wird.

Services werden mithilfe von Dependency-Injection verteilt, somit ist der Code in den einzelnen Modulen und Komponenten
viel sauberer.

Werden Änderungen am Frontend gemacht, so wird die Seite automatisch neu geladen, dies braucht der Programmierer
selbst nicht zu tun.
