/**
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit, ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import { AppState } from './app.service';

import { MOBILE } from '../services/constants';
import { views } from './app-nav-views';
import { MdSidenav } from '@angular/material';

/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.scss'
  ],
  templateUrl: './app.component.html',
  providers: []
})

export class AppComponent implements OnInit {
  @ViewChild('sidenav') public sidenav: MdSidenav;
  public mobile = MOBILE;
  public sideNavMode = MOBILE ? 'over' : 'side';
  public views = views;

  constructor(public appState: AppState,
              public route: ActivatedRoute,
              public router: Router) {
  }

  public ngOnInit() {
    console.log('Initial App State', this.appState.state);
  }

  public activateEvent(event) {
    if (ENV === 'development') {
      console.log('Activate Event:', event);
    }
  }

  public deactivateEvent(event) {
    if (ENV === 'development') {
      console.log('Deactivate Event', event);
    }
  }
}
