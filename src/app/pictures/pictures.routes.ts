import { ImagesComponent } from './images/images.component';
import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '', children: [
    {path: '', component: ImagesComponent}
  ],
  },
];
