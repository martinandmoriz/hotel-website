import gql from 'graphql-tag';

export const AddCustomerMutation = gql`
  mutation AddCustomer(
      $firstName: String!,
      $lastName: String!,
      $emailAddress: String,
      $phoneNumber: String
  ) {
    addCustomer(
      firstName: $firstName,
      lastName: $lastName,
      emailAddress: $emailAddress,
      phoneNumber: $phoneNumber
    ) {
      id
    }
  }
`;

export const AddReservationMutation = gql`
  mutation AddReservation(
    $customerId: String!,
    $from: String!,
    $to: String!,
    $floor: String,
    $extras: [String],
    $message: String
  ) {
    addReservation(
      customerId: $customerId,
      from: $from,
      to: $to,
      floor: $floor,
      extras: $extras,
      message: $message
    ) {
      id
    }
  }
`;
