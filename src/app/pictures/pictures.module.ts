import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import {
  MdCardModule,
  MdListModule,
  MdInputModule,
  MdButtonModule,
  MdSnackBarModule
} from '@angular/material';

import { ROUTES } from './pictures.routes';

import { ImagesComponent } from './images/images.component';

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    ImagesComponent,
  ],
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MdCardModule,
    MdListModule,
    MdInputModule,
    MdButtonModule,
    MdSnackBarModule,
    // ApolloModule.forRoot(client)
  ],
})
export class PicturesModule {
  public static routes = ROUTES;
}
