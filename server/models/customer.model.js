'use strict';

import Promise from 'bluebird';
import mongoose from 'mongoose';
import APIError from '../helpers/APIError';
import httpStatus from 'http-status';

const CustomerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  emailAddress: {
    type: String,
  },
  phoneNumber: {
    type: String,
    match: [/^((\+|00)\d\d)?\d{7,10}$/, 'The value of {PATH} ({VALUE}) is not a valid phone number.'],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

CustomerSchema.method({});

CustomerSchema.statics = {

  /**
   * Get a customer by id
   * @param {number} id
   * @returns {Promise<Customer, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((customer) => {
        if (customer) {
          return customer;
        }
        const err = new APIError('Customer not found.', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<Customer[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }

};

/**
 * @typedef CustomerSchema Customer
 */
export default mongoose.model('Customer', CustomerSchema);
