import { ContentComponent } from './content/content.component';
import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '', children: [
    {path: '', component: ContentComponent}
  ],
  },
];
