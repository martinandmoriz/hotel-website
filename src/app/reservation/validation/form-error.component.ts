import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
  selector: 'show-errors',
  template: `
    <ul *ngIf="shouldShowErrors()">
      <li style="color: red" *ngFor="let error of listOfErrors()">{{error}}</li>
    </ul>
  `,
})
export class FormErrorComponent {

  private static readonly errorMessages = {
    required: () => 'This field is required',
    minlength: (params) => 'The min number of characters is ' + params.requiredLength,
    maxlength: (params) => 'The max allowed number of characters is ' + params.requiredLength,
    pattern: (params) => 'The required pattern is: ' + params.requiredPattern,
    toAfterFrom: (params) => params.message,
  };

  private static getMessage(type: string, params: any) {
    return FormErrorComponent.errorMessages[type](params);
  }

  @Input()
  public control: AbstractControlDirective | AbstractControl;

  public shouldShowErrors(): boolean {
    return this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched);
  }

  public listOfErrors(): string[] {
    return Object.keys(this.control.errors)
      .map((field) => FormErrorComponent.getMessage(field, this.control.errors[field]));
  }
}
