import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormGroup, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[toAfterFrom]',
  providers: [{provide: NG_VALIDATORS, useExisting: ToAfterFromValidatorDirective, multi: true}]
})
export class ToAfterFromValidatorDirective implements Validator {

  public validate(form: FormGroup): ValidationErrors | null {
    const fromControl = form.get('from');
    const toControl = form.get('to');

    if (fromControl != null && toControl != null) {
      const from = Date.parse(fromControl.value);
      const to = Date.parse(toControl.value);
      let error = null;

      if (to <= from) {
        error = 'From must be before to.';
      }

      const message = {
        toAfterFrom: {
          message: error,
        },
      };

      return error ? message : null;
    }

    return null;
  }
}
