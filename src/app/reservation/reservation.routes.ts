import { Routes } from '@angular/router';
import { ReservationFormComponent } from './reservation-new/reservation-new.component';
import { MyReservationsComponent } from './reservation-list/reservation-list.component';

export const ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MyReservationsComponent,
      },
      {
        path: 'new',
        component: ReservationFormComponent,
      },
    ],
  }
];
