import { Component, OnInit } from '@angular/core';
import { ICarouselConfig, AnimationConfig } from 'angular4-carousel';
import { ImageService } from '../image.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'content',
  styleUrls: ['./content.component.scss'],
  templateUrl: './content.component.html',
})
export class ContentComponent implements OnInit {

  protected imageSources: Subject<string[]>;
  protected imageSourcesObservable: Observable<string[]>;

  protected config: ICarouselConfig = {
    verifyBeforeLoad: true,
    log: false,
    animation: true,
    animationType: AnimationConfig.SLIDE_OVERLAP,
    autoplay: true,
    autoplayDelay: 2000,
    stopAutoplayMinWidth: 768
  };

  constructor(private imageService: ImageService) {
    this.imageSources = new Subject();
    this.imageSourcesObservable = this.imageSources.asObservable();
  }

  public ngOnInit() {
    this.imageService.get()
      .result()
      .then((result) => {
        this.imageSources.next(result.data.images);
      })
      .catch((e) => {
        console.log('ImageService error:');
        console.log(e);
      });
  }
}
