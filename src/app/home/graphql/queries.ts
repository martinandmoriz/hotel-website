import gql from 'graphql-tag';

export const GetImagesQuery = gql`
  query Images {
    images {
      uri
      description
    }
  }
`;
